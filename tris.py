from __future__ import annotations
import curses
import sys
from random import choice
from typing import Union, Tuple
from time import time

BLOCK_CHAR = '█'
class Tetromino:
    def __init__(
            self,
            shape: list[str],
            position: Union[Tuple[int, int], None] = None,
            max_x: int = 9,
            max_y: int = 19,
            window: Union[curses.window, None] = None,
    ):
        self.shape = shape
        if position is None:
            self.position = (0, 0)
        else:
            self.position = position
        self.max_x = max_x
        self.max_y = max_y
        self.window = window

    def __str__(self):
        s = "\n"
        for row in self.shape:
            s += row + "\n"
        return s

    def rotate(self) -> Tetromino:
        new_shape = []
        y_size = len(self.shape)
        x_size = len(self.shape[0])
        for x in range(x_size):
            new_shape += [
                "".join(self.shape[y_size-y-1][x]
                        for y in range(y_size))
            ]
        if self.position[1] + len(new_shape[0]) - 1 > self.max_x:
            return self
        if self.position[0] + len(new_shape) - 1 > self.max_y:
            return self
        else:
            return Tetromino(
                new_shape,
                position=self.position,
                window=self.window
            )

    def left(self) -> Tetromino:
        if self.position[1] == 0:
            return self
        else:
            return Tetromino(
                self.shape,
                position=(self.position[0], self.position[1] - 1),
                window=self.window
            )

    def right(self) -> Tetromino:
        if self.position[1] + len(self.shape[0]) - 1 == self.max_x:
            return self
        else:
            return Tetromino(
                self.shape,
                position=(self.position[0], self.position[1] + 1),
                window=self.window
            )

    def down(self) -> Tetromino:
        if self.position[0] + len(self.shape) - 1 == self.max_y:
            return self
        else:
            return Tetromino(
                self.shape,
                position=(self.position[0] + 1, self.position[1]),
                window=self.window
            )

    def draw(self):
        if self.window is not None:
            for row, line in enumerate(self.shape):
                for column, value in enumerate(line):
                    if value != ' ':
                        color = ord(value) - 64
                        self.window.addstr(
                            row + self.position[0],
                            column + self.position[1],
                            BLOCK_CHAR,
                            curses.color_pair(color)
                        )


class Container:
    def __init__(
            self,
            parent_window,
            height=20,
            width=10,
            begin_y=2,
            begin_x=5
    ):
        self.height = height
        self.width = width
        self.window = parent_window.subwin(height+2, width+2, begin_y, begin_x)
        self.tetromino_subwin = self.window.subwin(
            height + 1,
            width + 1,
            begin_y + 1,
            begin_x + 1
        )
        self.lines = ["." * width] * height
        self.looping = False
        self.reset_tetromino()
        self.window.keypad(True)

    def reset_tetromino(self) -> bool:
        shapes = [
            [
                "A ",
                "AA",
                " A",
            ],
            [
                " B",
                "BB",
                "B ",
            ],
            [
                "B",
                "B",
                "B",
                "B",
            ],
            [
                "CC",
                "CC",
            ],
            [
                "D ",
                "DD",
                "D ",
            ],
            [
                "EE",
                "E ",
                "E ",
            ],
            [
                "FF",
                " F",
                " F",
            ],

        ]
        new_tetromino = Tetromino(
            choice(shapes),
            position=(0, 4),
            max_y=self.height-1,
            max_x=self.width-1,
            window=self.tetromino_subwin
        )
        self.tetromino = new_tetromino
        if self.intersects(new_tetromino):
            return False
        else:
            return True

    def draw(self):
        self.window.border()
        for row, line in enumerate(self.lines):
            for col, value in enumerate(line):
                if ord(value) >= 65 and ord(value) <= (65 + 26):
                    character = BLOCK_CHAR
                    color = ord(value) - 64
                else:
                    color = 0
                    character = value
                self.window.addch(row+1, col+1, character, curses.color_pair(color))
        self.tetromino.draw()

    def intersects(self, tetromino):
        for row, line in enumerate(tetromino.shape):
            for column, value in enumerate(line):
                y = tetromino.position[0] + row
                x = tetromino.position[1] + column
                if value != ' ' and self.lines[y][x] != '.':
                    return True
        return False

    def clear_lines(self):
        keep_going = True
        while keep_going:
            keep_going = False
            for row, line in enumerate(self.lines):
                if '.' not in line:
                    self.lines = (
                        ["." * self.width] +
                        self.lines[:row] +
                        self.lines[row + 1:]
                    )
                    keep_going = True
                    break

    def up(self):
        next_tetromino = self.tetromino.rotate()
        if not self.intersects(next_tetromino):
            self.tetromino = next_tetromino

    def down(self):
        next_tetromino = self.tetromino.down()
        if (
                (self.tetromino.position != next_tetromino.position)
                and not self.intersects(next_tetromino)
        ):
            self.tetromino = next_tetromino
        else:
            for row, line in enumerate(self.tetromino.shape):
                for column, value in enumerate(line):
                    y = self.tetromino.position[0] + row
                    x = self.tetromino.position[1] + column
                    if value != ' ':
                        self.lines[y] = (
                            self.lines[y][:x]
                            + value
                            + self.lines[y][x + 1:]
                        )
            self.clear_lines()
            if not self.reset_tetromino():
                self.looping = False

    def left(self):
        next_tetromino = self.tetromino.left()
        if not self.intersects(next_tetromino):
            self.tetromino = next_tetromino

    def right(self):
        next_tetromino = self.tetromino.right()
        if not self.intersects(next_tetromino):
            self.tetromino = next_tetromino

    def loop(self):
        self.window.timeout(0)
        self.looping = True
        last_time = time()
        while self.looping:
            if (time() - last_time) >= 0.5:
                last_time = time()
                self.down()
            self.draw()
            try:
                k = self.window.getkey()
                if k == "q":
                    sys.exit(0)
                elif k == "KEY_UP":
                    self.up()
                elif k == "KEY_LEFT":
                    self.left()
                elif k == "KEY_RIGHT":
                    self.right()
                elif k == "KEY_DOWN":
                    self.down()
            except curses.error:
                pass


def main(stdscr):
    for pair, color in enumerate([
        curses.COLOR_RED, 
        curses.COLOR_YELLOW, 
        curses.COLOR_GREEN, 
        curses.COLOR_BLUE, 
        curses.COLOR_CYAN, 
        curses.COLOR_MAGENTA,
        curses.COLOR_WHITE,
        ]):
        curses.init_pair(pair + 1, color, curses.COLOR_BLACK)
    curses.curs_set(0)
    c = Container(stdscr)
    c.loop()


if __name__ == '__main__':
    curses.wrapper(main)
