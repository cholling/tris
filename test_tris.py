from tris import Tetromino

def test_tetromino():
    shape = [
        " ##",
        "## ",
    ]
    t = Tetromino(shape)
    assert t.shape == shape


def test_rotate_tetromino():
    shape = [
        " ##",
        "## ",
    ]
    rotated_shape = [
        "# ",
        "##",
        " #",
    ]
    t = Tetromino(shape)
    assert t.rotate().shape == rotated_shape

    # verify no violation of lower bound
    t = Tetromino(shape, position=(2, 2), max_y=3)
    assert t.rotate().shape == shape

    # verify no violation of right bound
    t = Tetromino(rotated_shape, position=(2, 3), max_x=4)
    assert t.rotate().shape == rotated_shape



def test_move_tetromino_left():
    shape = [
        " ##",
        "## ",
    ]
    original_position = (2, 2)
    t = Tetromino(shape, position=original_position)
    assert t.position == (2, 2)
    t_left = t.left()
    assert t_left.position == (2, 1)
    t_rotate = t.rotate()
    assert t_rotate.position == (2, 2)
    t_left_rotate = t_left.rotate()
    assert t_left_rotate.position == (2, 1)

    # test left boundary
    t = Tetromino(shape, position=(2, 0))
    t_left = t.left()
    assert t_left.position == (2, 0)



def test_move_tetromino_right():
    shape = [
        " ##",
        "## ",
    ]
    original_position = (2, 2)
    t = Tetromino(shape, position=original_position)
    assert t.position == (2, 2)
    t_right = t.right()
    assert t_right.position == (2, 3)
    t_rotate = t.rotate()
    assert t_rotate.position == (2, 2)
    t_right_rotate = t_right.rotate()
    assert t_right_rotate.position == (2, 3)

    # test right boundary
    t = Tetromino(shape, position=(2, 2), max_x=4)
    t_right = t.right()
    assert t_right.position == (2, 2)

def test_move_tetromino_down():
    shape = [
        " ##",
        "## ",
    ]
    original_position = (2, 2)
    t = Tetromino(shape, position=original_position)
    assert t.position == (2, 2)
    t_down = t.down()
    assert t_down.position == (3, 2)
    t_rotate = t.rotate()
    assert t_rotate.position == (2, 2)
    t_down_rotate = t_down.rotate()
    assert t_down_rotate.position == (3, 2)

    # test lower boundary
    t = Tetromino(shape, position=(2, 2), max_y=3)
    t_down = t.down()
    assert t_down.position == (2, 2)
